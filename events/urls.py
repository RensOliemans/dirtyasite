from events import views
from rest_framework.routers import DefaultRouter

app_name = 'events'

router = DefaultRouter()
router.register(r'', views.EventViewSet)
router.register(r'registrations', views.RegistrationViewSet)

urlpatterns = router.urls
