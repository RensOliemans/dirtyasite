from decimal import Decimal

from django.test import TestCase
from django.utils import timezone

from dirtyasite.test_utils import create_event, create_resident
from events.models import Registration
from residents.models import Resident

DEFAULT_DATE = timezone.now().date()


class RegistrationCostTests(TestCase):
    def test_no_costs_made_correctly(self):
        """
        Tests that a person registering for an event without making costs, just gets the costs
        per person of that event
        """
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario", DEFAULT_DATE, 6, DEFAULT_DATE, False)

        # Act
        Registration.objects.create(event=event, user=ollie, costs=12.6, factor=1)
        Registration.objects.create(event=event, user=mario, costs=0.00, factor=1)

        # Assert
        ollie.refresh_from_db()
        mario.refresh_from_db()
        self.assertEqual(ollie.balance, Decimal('6.3'))
        self.assertEqual(mario.balance, Decimal('-6.3'))

    def test_factor_two(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario", DEFAULT_DATE, 6, DEFAULT_DATE, False)

        # Act
        Registration.objects.create(event=event, user=ollie, costs=12.6, factor=1)
        Registration.objects.create(event=event, user=mario, costs=0.00, factor=2)

        # Assert
        ollie.refresh_from_db()
        mario.refresh_from_db()
        self.assertEqual(ollie.balance, Decimal('8.4'))
        self.assertEqual(mario.balance, Decimal('-8.4'))

    def test_two_people_costs(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario", DEFAULT_DATE, 6, DEFAULT_DATE, False)

        # Act
        Registration.objects.create(event=event, user=ollie, costs=12.6, factor=1)
        Registration.objects.create(event=event, user=mario, costs=3.50, factor=1)

        # Assert
        ollie.refresh_from_db()
        mario.refresh_from_db()
        self.assertEqual(ollie.balance, Decimal('4.55'))
        self.assertEqual(mario.balance, Decimal('-4.55'))

    def test_one_person_costs(self):
        # Arrange
        event = create_event()
        ollie = create_resident()

        # Act
        Registration.objects.create(event=event, user=ollie, costs=12.6, factor=2)

        # Assert
        ollie.refresh_from_db()
        self.assertEqual(ollie.balance, Decimal(0))

    def test_three_people_costs(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario", DEFAULT_DATE, 6, DEFAULT_DATE, False)
        frank = create_resident("Frank", DEFAULT_DATE, 4, DEFAULT_DATE, False)

        # Act
        Registration.objects.create(event=event, user=ollie, costs=12.6, factor=1)
        Registration.objects.create(event=event, user=mario, costs=76.3, factor=13)
        Registration.objects.create(event=event, user=frank, costs=0.80, factor=2)

        # Assert
        ollie.refresh_from_db()
        mario.refresh_from_db()
        frank.refresh_from_db()
        self.assertEqual(ollie.balance, Decimal('6.99'))
        self.assertEqual(mario.balance, Decimal('3.42'))
        self.assertEqual(frank.balance, Decimal('-10.41'))

    def test_rounded_calculation(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario", DEFAULT_DATE, 6, DEFAULT_DATE, False)
        frank = create_resident("Frank", DEFAULT_DATE, 4, DEFAULT_DATE, False)

        # Act
        Registration.objects.create(event=event, user=ollie, costs=1, factor=1)
        Registration.objects.create(event=event, user=mario, costs=0, factor=1)
        Registration.objects.create(event=event, user=frank, costs=0, factor=1)

        # Assert
        ollie.refresh_from_db()
        mario.refresh_from_db()
        frank.refresh_from_db()
        self.assertEqual(ollie.balance, Decimal('0.67'))
        self.assertEqual(mario.balance, Decimal('-0.34'))
        self.assertEqual(frank.balance, Decimal('-0.33'))
        self.assertEqual(0, sum([resident.balance for resident in Resident.objects.all()]))


class RegistrationCookingPointsTest(TestCase):
    def test_no_cook(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario")

        # Act
        Registration.objects.create(event=event, user=ollie)
        Registration.objects.create(event=event, user=mario)

        # Assert
        self.assertEqual(ollie.cooking_points, 0)

    def test_simple_cooking_points(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario", DEFAULT_DATE, 6, DEFAULT_DATE, False)
        frank = create_resident("Frank", DEFAULT_DATE, 4, DEFAULT_DATE, False)
        sandy = create_resident("Sandy", DEFAULT_DATE, 7, DEFAULT_DATE, False)

        # Act
        Registration.objects.create(event=event, user=ollie, cook=True)
        Registration.objects.create(event=event, user=mario)
        Registration.objects.create(event=event, user=frank)
        Registration.objects.create(event=event, user=sandy)

        # Assert
        self.assertEqual(ollie.cooking_points, 3)
        self.assertEqual(mario.cooking_points, -1)
        self.assertEqual(frank.cooking_points, -1)
        self.assertEqual(sandy.cooking_points, -1)

    def test_cooking_points_two_cooks(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario", DEFAULT_DATE, 6, DEFAULT_DATE, False)
        frank = create_resident("Frank", DEFAULT_DATE, 4, DEFAULT_DATE, False)
        sandy = create_resident("Sandy", DEFAULT_DATE, 7, DEFAULT_DATE, False)
        vicky = create_resident("Vicky", DEFAULT_DATE, 3, DEFAULT_DATE, True)

        # Act
        Registration.objects.create(event=event, user=ollie, cook=True)
        Registration.objects.create(event=event, user=mario)
        Registration.objects.create(event=event, user=frank, cook=True)
        Registration.objects.create(event=event, user=sandy)
        Registration.objects.create(event=event, user=vicky)

        # Assert
        self.assertEqual(ollie.cooking_points, Decimal('1.5'))
        self.assertEqual(frank.cooking_points, Decimal('1.5'))
        self.assertEqual(mario.cooking_points, -1)
        self.assertEqual(sandy.cooking_points, -1)
        self.assertEqual(vicky.cooking_points, -1)

    def test_cooking_points_two_cooks_more_factor(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario", DEFAULT_DATE, 6, DEFAULT_DATE, False)
        frank = create_resident("Frank", DEFAULT_DATE, 4, DEFAULT_DATE, False)
        sandy = create_resident("Sandy", DEFAULT_DATE, 7, DEFAULT_DATE, False)
        vicky = create_resident("Vicky", DEFAULT_DATE, 3, DEFAULT_DATE, True)

        # Act
        Registration.objects.create(event=event, user=ollie, factor=2, cook=True)
        Registration.objects.create(event=event, user=mario)
        Registration.objects.create(event=event, user=frank, cook=True)
        Registration.objects.create(event=event, user=sandy)
        Registration.objects.create(event=event, user=vicky, factor=2)

        # Assert
        self.assertEqual(ollie.cooking_points, 2)
        self.assertEqual(frank.cooking_points, 2)
        self.assertEqual(mario.cooking_points, -1)
        self.assertEqual(sandy.cooking_points, -1)
        self.assertEqual(vicky.cooking_points, -2)

    def test_cooking_points_not_neatly_roundable(self):
        # Arrange
        event = create_event()
        ollie = create_resident()
        mario = create_resident("Mario")
        frank = create_resident("Frank")
        sandy = create_resident("Sandy")

        # Act
        Registration.objects.create(event=event, user=ollie, cook=True)
        Registration.objects.create(event=event, user=mario, cook=True)
        Registration.objects.create(event=event, user=frank, cook=True)
        Registration.objects.create(event=event, user=sandy, cook=False)

        # Assert
        self.assertEqual(ollie.cooking_points, Decimal('0.33'))
        self.assertEqual(mario.cooking_points, Decimal('0.34'))
        self.assertEqual(frank.cooking_points, Decimal('0.33'))
        self.assertEqual(sandy.cooking_points, -1)
        self.assertEqual(0, sum([resident.cooking_points for resident in Resident.objects.all()]))
