from events.models import Event, Registration
from rest_framework import serializers


class EventSerializer(serializers.HyperlinkedModelSerializer):
    registration = serializers.HyperlinkedRelatedField(many=True,
                                                       read_only=True,
                                                       view_name='events:registration-detail')

    class Meta:
        model = Event
        fields = ('id', 'title', 'date', 'description', 'deadline', 'registration')


class RegistrationSerializer(serializers.ModelSerializer):
    event = serializers.ReadOnlyField(source='event.title')
    date = serializers.ReadOnlyField(source='event.date')
    user = serializers.ReadOnlyField(source='user.nickname')

    class Meta:
        model = Registration
        fields = ('user', 'event', 'date', 'costs', 'factor', 'cook', 'comment', 'id')
