# Generated by Django 2.2.6 on 2019-11-11 16:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('residents', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=30, verbose_name='title')),
                ('date', models.DateTimeField(verbose_name='date')),
                ('deadline', models.DateTimeField(verbose_name='deadline')),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ('date',),
            },
        ),
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('costs', models.DecimalField(decimal_places=2, default=0, help_text='The costs that this person made for the event', max_digits=5, verbose_name='costs')),
                ('factor', models.DecimalField(decimal_places=0, default=1, help_text='The amount of times you participate in this event', max_digits=2, verbose_name='factor')),
                ('cook', models.BooleanField(default=False, verbose_name='cook')),
                ('comment', models.TextField(blank=True, null=True, verbose_name='comment')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='date_created')),
                ('date_modified', models.DateTimeField(auto_now=True, verbose_name='date_modified')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='registration', to='events.Event')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='residents.Resident')),
            ],
            options={
                'verbose_name': 'registration',
                'verbose_name_plural': 'registrations',
                'unique_together': {('event', 'user')},
            },
        ),
    ]
