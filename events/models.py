import inflect
from datetime import datetime, timedelta
from django.db import models
from django.db.models import Sum

from residents.models import Resident


class EventQuerySet(models.QuerySet):
    today_date = datetime.today().date()

    def today(self):
        return self.get(date__date=self.today_date)

    def future(self):
        return self.filter(date__gte=self.today_date)

    def past(self):
        return self.filter(date__lt=self.today_date)

    def default(self):
        current_week_number = self.today_date.isocalendar()[2]
        last_week_begin = self.today_date - timedelta(days=current_week_number + 6)  # Last Monday
        next_week_end = self.today_date + timedelta(days=15 - current_week_number)  # Next Friday
        return self.filter(date__gt=last_week_begin, date__lt=next_week_end)

    def three_weeks(self, date):
        week_number = date.isocalendar()[2]
        last_week_begin = date - timedelta(days=week_number + 6)  # Last Monday
        next_week_end = date + timedelta(days=15 - week_number)  # Next Friday
        return self.filter(date__gt=last_week_begin, date__lt=next_week_end)

    def last_month(self):
        return self.filter(date__gt=self.today_date - timedelta(days=30), date__lt=self.today_date)

    def next_week(self):
        return self.filter(date__gt=self.today_date, date__lt=self.today_date + timedelta(weeks=1))

    def has_cook(self):
        return self.filter(registration__in=Registration.objects.filter(cook=True))

    def cooked_by(self, resident):
        return self.filter(registration__in=Registration.objects.filter(cook=True, user=resident))


# noinspection PyPep8Naming
class Event(models.Model):
    """
    Base event model.
    """
    title = models.CharField(
        max_length=30,
        verbose_name="title"
    )
    date = models.DateTimeField(verbose_name="date")
    deadline = models.DateTimeField(verbose_name="deadline")
    description = models.TextField()

    _costs_per_person = dict()
    _points_per_person = dict()

    objects = EventQuerySet.as_manager()

    def __str__(self):
        return self.title

    def __repr__(self):
        return f"<Event: {self.title}. Date: {self.date}, description: {self.description}>"

    def save(self, *args, **kwargs):
        if not self.deadline:
            self.deadline = self.date
        super().save(*args, **kwargs)

    def divide_points(self):
        point_list = list(self._compute_divisions(self.points_to_be_given, len(self.cooks)))
        for index, cook in enumerate(self.cooks):
            self._points_per_person[cook] = point_list[index]

        for participant in self.not_cooks:
            _, _, factor = self._get_information(participant)
            self._points_per_person[participant] = -factor

    def divide_costs(self):
        cost_list = list(self._compute_divisions(self.costs_total, self.participant_count))
        index = 0
        for participant in self.participants:
            registration, costs, factor = self._get_information(participant)
            amount_to_pay = sum(cost_list[index:index + factor])
            total_amount = costs - amount_to_pay

            self._costs_per_person[participant] = total_amount
            index += factor

    def _get_information(self, participant):
        registration = Registration.objects.get(event=self, user=participant)
        return registration, registration.costs, int(registration.factor)

    def costs_of_person(self, person):
        return self._costs_per_person[person] if person in self._costs_per_person else 0

    def points_per_person(self, person):
        return self._points_per_person[person] if person in self._points_per_person else 0

    @property
    def guest_count(self):
        total_participants = self.registration.all().aggregate(Sum('factor'))['factor__sum']
        house_participants = self.registration.count()  # Without the extra guests
        return total_participants - house_participants

    @property
    def registrations(self):
        return self.registration.all()

    @property
    def participants(self):
        for registration in self.registrations:
            yield registration.user

    @property
    def participant_count(self):
        return self.registration.all().aggregate(Sum('factor'))['factor__sum'] or 0

    @property
    def costs_total(self):
        return self.registration.all().aggregate(Sum('costs'))['costs__sum']

    @property
    def points_to_be_given(self):
        return self.registration.filter(cook=False).aggregate(Sum('factor'))['factor__sum'] or 0

    @property
    def cooks(self):
        registrations = self.registration.filter(cook=True)
        return [registration.user for registration in registrations]

    @property
    def not_cooks(self):
        registrations = self.registration.filter(cook=False)
        return [registration.user for registration in registrations]

    @staticmethod
    def _compute_divisions(total, divider):
        while divider > 0:
            amount = round(total / divider, 2)
            yield amount
            total -= amount
            divider -= 1

    class Meta:
        ordering = ("date",)


class Registration(models.Model):
    event = models.ForeignKey(
        to=Event,
        on_delete=models.CASCADE,
        related_name="registration"
    )
    user = models.ForeignKey(
        to=Resident,
        on_delete=models.PROTECT
    )
    costs = models.DecimalField(
        default=0,
        max_digits=5,
        decimal_places=2,
        verbose_name="costs",
        help_text="The costs that this person made for the event"
    )
    factor = models.DecimalField(
        default=1,
        max_digits=2,
        decimal_places=0,
        verbose_name="factor",
        help_text="The amount of times you participate in this event"
    )
    cook = models.BooleanField(
        default=False,
        verbose_name="cook"
    )
    comment = models.TextField(
        blank=True,
        null=True,
        verbose_name="comment"
    )
    date_created = models.DateTimeField(
        auto_now_add=True,
        verbose_name="date_created"
    )
    date_modified = models.DateTimeField(
        auto_now=True,
        verbose_name="date_modified"
    )

    # Used for pluralisation
    p = inflect.engine()

    def __str__(self):
        return (f"Event: {self.event}. User: {self.user}, {self.factor} "
                f"{self.p.plural('time', self.factor)}")

    def __repr__(self):
        return f"<Registration. Event: {self.event}. User: {self.user}, {self.factor} " \
            f"{self.p.plural('time', self.factor)}. Costs: {self.costs}. Cook: {self.cook}>"

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.event.costs_total:
            self.event.divide_costs()

        if self.event.cooks:
            self.event.divide_points()

    class Meta:
        verbose_name = "registration"
        verbose_name_plural = "registrations"
        unique_together = [['event', 'user']]

    def _determine_costs_of_registration(self, amount_paid, amount_to_pay):
        pass
