from django.utils import timezone
from factory import Faker, Iterator, django

from events import models
from residents import model_factory
from residents import models as resident_models

if not resident_models.Resident.objects.exists():
    model_factory.ResidentFactory()


class EventFactory(django.DjangoModelFactory):
    class Meta:
        model = models.Event

    title = Faker('word')
    date = Faker('date_time', tzinfo=timezone.utc)
    description = Faker('text', max_nb_chars=40)


class RegistrationFactory(django.DjangoModelFactory):
    class Meta:
        model = models.Registration

    event = Iterator(models.Event.objects.all())
    user = Iterator(resident_models.Resident.objects.all())

    costs = Faker('pydecimal', max_value=100, min_value=1, right_digits=2)
    factor = Faker('pydecimal', max_value=3, min_value=1, right_digits=0)
    cook = Faker('pybool')
    comment = Faker('text', max_nb_chars=40)
