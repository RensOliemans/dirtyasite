from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from residents.models import User, Resident


class ResidentInline(admin.StackedInline):
    model = Resident
    can_delete = False


@admin.register(User)
class UserAdmin(UserAdmin):
    inlines = (ResidentInline,)
    list_display = ('username',
                    'email',
                    'first_name',
                    'last_name',
                    'room',
                    'birthday',
                    'start',
                    'balance',
                    'cooking_points',
                    'feut',)
    # ordering = ('resident__start',)

    def get_resident(self, user):
        return Resident.objects.get(user=user)

    def room(self, obj):
        return self.get_resident(obj).room

    def birthday(self, obj):
        return self.get_resident(obj).birthday

    def start(self, obj):
        return self.get_resident(obj).start

    def balance(self, obj):
        return self.get_resident(obj).balance

    def cooking_points(self, obj):
        return self.get_resident(obj).cooking_points

    def feut(self, obj):
        return self.get_resident(obj).feut
