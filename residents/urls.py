from residents import views
from rest_framework.routers import DefaultRouter

app_name = 'residents'

# This router automatically creates urls for the views
router = DefaultRouter()
router.register(r'', views.ResidentViewSet)

urlpatterns = router.urls
