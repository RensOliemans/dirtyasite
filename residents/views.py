from residents.models import Resident
from residents.serializers import ResidentSerializer
from rest_framework import permissions, viewsets


class ResidentViewSet(viewsets.ModelViewSet):
    queryset = Resident.objects.residents()
    serializer_class = ResidentSerializer
    permission_classes = (permissions.IsAuthenticated,)
