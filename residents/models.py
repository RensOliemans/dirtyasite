from django.contrib.auth.models import AbstractUser, UserManager
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from factory.django import get_model


class ResidentManager(UserManager):
    def get_by_natural_key(self, username):
        case_insensitive_username = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username: username})


class User(AbstractUser):
    objects = ResidentManager()


class ResidentQuerySet(models.QuerySet):
    def feuten(self):
        return self.filter(feut=True)

    def residents(self):
        return self.all().order_by('start')


class Resident(models.Model):
    """
    Model inheriting from Django's user. This is to make authentication easy, and adds possible
    custom fields and properties
    """
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
        verbose_name="user",
        related_name="auth_user",
    )
    nickname = models.CharField(
        max_length=30,
        unique=True,
        null=True,
        blank=True,
        verbose_name="nickname"
    )
    start = models.DateField(
        verbose_name="start",
        help_text="The date when the resident moved in"
    )
    room = models.PositiveIntegerField(
        validators=[MinValueValidator(3), MaxValueValidator(10)],
        verbose_name="room_number"
    )
    birthday = models.DateField(
        null=True,
        blank=True,
        verbose_name="birthday"
    )
    feut = models.BooleanField(
        default=True,
        verbose_name="is feut"
    )

    @property
    def balance(self):
        event_model = get_model('events', 'Event')
        costs = sum([event.costs_of_person(self)
                     for event in event_model.objects.filter(registration__user=self)])
        return costs

    @property
    def cooking_points(self):
        event_model = get_model('events', 'Event')
        points = sum([event.points_per_person(self)
                      for event in event_model.objects.filter(registration__user=self)])
        return points

    objects = ResidentQuerySet.as_manager()

    def save(self, *args, **kwargs):
        if self.nickname and not self.user.username:
            self.user.username = self.nickname
        self.user.username = self.user.username.lower()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.nickname or self.user.get_full_name()

    def __repr__(self):
        return f"<Resident: {self.nickname} (username: {self.user.username}). Room: {self.room}, " \
            f"balance: {self.balance}. Cooking points: {self.cooking_points}>"
