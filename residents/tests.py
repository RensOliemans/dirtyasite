# Create your tests here.
from django.test.testcases import TestCase
import datetime
from residents.models import Resident, User


def create_user(username='testuser'):
    user = User(username=username)
    user.save()
    res = Resident(user=user, start=datetime.datetime(year=2019, month=1, day=1), room=3)
    res.save()
    return res


class UsernameCaseInsentivieTest(TestCase):
    def test_username_lowercase(self):
        username = 'ollie'
        user = create_user(username)

        self.assertEqual(username, user.user.username)

    def test_username_uppercase(self):
        username = 'OLLIE'
        user = create_user(username)

        self.assertEqual(username.lower(), user.user.username)
