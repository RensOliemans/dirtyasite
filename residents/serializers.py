from django.contrib.auth.models import Group
from residents.models import Resident
from rest_framework import serializers


class ResidentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resident
        fields = ('id', 'nickname', 'start', 'room', 'birthday', 'feut', 'cooking_points', 'balance')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')
