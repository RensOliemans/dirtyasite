from django.utils import timezone
from factory import Faker, django, fuzzy

from residents import models


class ResidentFactory(django.DjangoModelFactory):
    class Meta:
        model = models.Resident

    nickname = Faker('first_name')
    username = nickname
    start = Faker('date_time', tzinfo=timezone.utc)
    room = fuzzy.FuzzyInteger(3, 10)
    birthday = Faker('date_time', tzinfo=timezone.utc)
    feut = Faker('pybool')
