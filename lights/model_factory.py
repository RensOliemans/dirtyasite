from factory import Faker, Iterator, LazyAttribute, django, fuzzy

from lights import models


def create_mapping(methods):
    index = 0
    final_dict = {}
    for method in models.CHANNELS.keys():
        if method not in methods:
            continue
        for i in range(models.CHANNELS[method]):
            final_dict[f'{index}'] = f'{method}'
            index += 1
    return final_dict


def determine_amount_of_channels(methods):
    channels = 0
    for method in models.CHANNELS.keys():
        if method not in methods:
            continue
        channels += models.CHANNELS[method]
    return channels


class FixtureFactory(django.DjangoModelFactory):
    class Meta:
        model = models.Fixture

    name = Faker('word')
    methods = Faker('words', nb=2, unique=True, ext_word_list=models.METHODS)
    channel_amount = LazyAttribute(lambda a: determine_amount_of_channels(a.methods))
    channel_method_mapping = LazyAttribute(lambda a: create_mapping(a.methods))


class LampFactory(django.DjangoModelFactory):
    class Meta:
        model = models.Lamp

    fixture = Iterator(models.Fixture.objects.all())

    name = Faker('word')
    start_channel = fuzzy.FuzzyInteger(0, 512)


if not models.Fixture.objects.exists():
    FixtureFactory()
