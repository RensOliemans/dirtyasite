from django.core.exceptions import ValidationError
from django.test import TestCase

from lights.models import Fixture, Lamp


def _create_fixture(name='LED', channel_amount=3, methods=None, mapping=None):
    if methods is None:
        methods = ['color']
    if mapping is None:
        mapping = {'0': 'color', '1': 'color', '2': 'color'}
    return Fixture.objects.create(name=name, methods=methods,
                                  channel_method_mapping=mapping)


class FixtureTests(TestCase):
    def test_create_fixture_one_method(self):
        f = Fixture(name='LED', methods=['color'],
                    channel_method_mapping={'0': 'color', '1': 'color', '2': 'color'})
        try:
            f.save()
        except ValidationError:
            self.fail("Saving failed when it should have been correct")

    def test_create_fixture_without_mapping(self):
        """
        Tests that the creation of a fixture without a mapping goes wrong
        """
        f = Fixture(name='LED', methods=['color'])
        self.assertRaises(ValidationError, f.save)

    def test_create_fixture_unknown_methods(self):
        f = Fixture(name='LED', methods=['henk'],
                    channel_method_mapping={'henk': '0'})
        self.assertRaisesMessage(
            ValidationError,
            "Item 1 in the array did not validate: Value \'henk\' is not a valid choice.",
            f.save)

    def test_create_fixture_wrong_method_in_mapping(self):
        f = Fixture(name='LED', methods=['color', 'strobe'],
                    channel_method_mapping={'0': 'color', '1': 'color', '2': 'color', '3': 'henk'})
        self.assertRaisesMessage(
            ValidationError,
            "Allowed methods: [\'color\', \'strobe\', \'music\']. Chosen method: \'henk\'",
            f.save
        )

    def test_create_fixture_wrong_amount_of_channels_in_mapping(self):
        f = Fixture(name='LED', methods=['color', 'strobe'],
                    channel_method_mapping={'0': 'color', '1': 'color', '2': 'strobe'})
        self.assertRaisesMessage(
            ValidationError,
            'Invalid amount of channels for the mapping. The mapping had 3 keys, '
            'but the amount of channels was 4',
            f.save
        )

    def test_create_fixture_wrong_amount_of_channels_for_methods(self):
        f = Fixture(name='LED', methods=['color', 'strobe'],
                    channel_method_mapping={'0': 'color', '1': 'color', '2': 'color'})
        self.assertRaisesMessage(
            ValidationError,
            "With the given methods ([\'color\', \'strobe\']) we expect 4 channels. "
            "However, the amount of channels given was 3",
            f.save
        )

    def test_create_fixture_too_high_channel(self):
        f = Fixture(name='LED', methods=['color'],
                    channel_method_mapping={'0': 'color', '1': 'color', '3': 'color'})
        self.assertRaisesMessage(
            ValidationError,
            "'The fixture should have 3 channels, but in the mapping a channel "
            "exists with a too high channel.'",
            f.save
        )

    def test_create_fixture_negative_channel(self):
        f = Fixture(name='LED', methods=['color'],
                    channel_method_mapping={'-1': 'color', '1': 'color', '2': 'color'})
        self.assertRaisesMessage(
            ValidationError,
            "'A channel exists with a negative number. "
            "The channels in the mapping should start from 0 and go to 2.'",
            f.save
        )


class LightTests(TestCase):
    def test_determine_channel(self):
        f = _create_fixture()
        light = Lamp.objects.create(name='LED 1', fixture=f, start_channel=1)
        self.assertEqual(light.determine_channels('color'), [1, 2, 3])

    def test_determine_channel_strobe(self):
        f = _create_fixture(channel_amount=4, methods=['color', 'strobe'],
                            mapping={'0': 'color', '1': 'color', '2': 'color', '3': 'strobe'})
        light = Lamp.objects.create(name='LED 1', fixture=f, start_channel=242)
        self.assertEqual(light.determine_channels('strobe'), [245])
