from django.contrib.postgres import fields
from django.contrib.postgres.fields import HStoreField
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from model_utils import Choices

from lights.exceptions import InvalidMethodError

CHANNELS = {
    'color': 3,
    'strobe': 1,
    'music': 1
}
METHODS = list(CHANNELS.keys())


class Fixture(models.Model):
    """
    This model is a fixture, a general layout for a light
    """
    MAX_CHANNELS = 20

    name = models.CharField(
        max_length=20,
        null=False,
        unique=True,
        help_text='The name of the fixture',
    )
    methods = fields.ArrayField(
        models.CharField(choices=Choices(*METHODS), max_length=20),
        default=list,
        help_text='A list of methods which the fixture supports',
    )
    channel_method_mapping = HStoreField(
        help_text='A mapping of channel (key) to method (value)'
    )

    @property
    def channel_amount(self):
        return sum([CHANNELS[method]
                    for method in set(self.channel_method_mapping.values())])

    @property
    def instances(self):
        return self.light.all()

    class Meta:
        ordering = ('name',)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def full_clean(self, exclude=None, validate_unique=True):
        super().full_clean(exclude, validate_unique)
        self._validate_int_channels()
        self._validate_correct_methods()
        self._validate_given_channels_amount()
        self._validate_method_channels_amount()
        self._validate_too_high_channel()
        self._validate_non_negative_channels()

    def _validate_int_channels(self):
        try:
            [int(channel) for channel in self.channel_method_mapping]
        except TypeError:
            raise ValidationError(f"All channels should be integers")

    def _validate_correct_methods(self):
        try:
            [CHANNELS[method] for method in self.channel_method_mapping.values()]
        except KeyError as e:
            raise ValidationError(f"Allowed methods: {METHODS}. Chosen method: {e}")

    def _validate_given_channels_amount(self):
        if len(set(self.channel_method_mapping)) != self.channel_amount:
            raise ValidationError(f"Invalid amount of channels for the mapping. The mapping had "
                                  f"{len(self.channel_method_mapping)} keys, but the amount of "
                                  f"channels was {self.channel_amount}")

    def _validate_method_channels_amount(self):
        expected_channels_from_method = sum([CHANNELS[method]
                                             for method in set(self.methods)])

        if expected_channels_from_method != self.channel_amount:
            raise ValidationError(f"With the given methods ({self.methods}) we expect "
                                  f"{expected_channels_from_method} channels. However, "
                                  f"the amount of channels given was {self.channel_amount}")

    def _validate_too_high_channel(self):
        if any([int(channel) >= self.channel_amount for channel in self.channel_method_mapping]):
            raise ValidationError(f"The fixture should have {self.channel_amount} channels, "
                                  f"but in the mapping a channel exists with a too high channel.")

    def _validate_non_negative_channels(self):
        if any([int(channel) < 0 for channel in self.channel_method_mapping]):
            raise ValidationError(f"A channel exists with a negative number. The channels in the "
                                  f"mapping should start from 0 and go to "
                                  f"{self.channel_amount - 1}.")

    def __str__(self):
        return f"Fixture {self.name} with {self.channel_amount} of channels and methods:" \
            f"{self.methods}."

    def __repr__(self):
        return f"<Fixture: {self.name}. Amount of channels: {self.channel_amount}. Methods: " \
            f"{self.methods}. Mapping: {self.channel_method_mapping}>"


class Lamp(models.Model):
    """
    This model is an instance of a fixture
    """
    name = models.CharField(
        max_length=20,
        null=False
    )
    fixture = models.ForeignKey(
        to=Fixture,
        related_name="light",
        on_delete=models.PROTECT,
        null=False
    )
    start_channel = models.PositiveIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(512)],
        null=False,
        unique=True,
    )

    @property
    def channels(self):
        return [channel for channel in range(self.start_channel,
                                             self.fixture.channel_amount + self.start_channel)]

    @property
    def methods(self):
        return self.fixture.methods

    class Meta:
        ordering = ('start_channel', 'name')

    def determine_channels(self, method_name):
        if method_name not in self.fixture.methods:
            raise InvalidMethodError(f"Available methods: {self.methods}. "
                                     f"Given method: {method_name}")

        mapping = self.fixture.channel_method_mapping
        return [self.start_channel + int(channel)
                for channel in mapping if mapping[channel] == method_name]

    def __str__(self):
        return f"Light with fixture: {self.fixture.name}, starting on channel {self.start_channel}"

    def __repr__(self):
        return f"<Light: {self.name}. Fixture: {self.fixture}. Start channel: {self.start_channel}>"
