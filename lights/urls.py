from django.urls import path
from rest_framework import routers

from lights import views

app_name = 'lights'

router = routers.SimpleRouter()
router.register(r'lamps', views.LampViewSet)
router.register(r'fixtures', views.FixtureViewSet)

urlpatterns = [
    path('', views.lamp_root, name='lights-root'),
]

urlpatterns += router.urls
