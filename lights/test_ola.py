import json
from unittest import mock

from django.test import TestCase
from lights.models import Fixture
from lights.ola_handler import OlaHandler


def create_fixture(name='LED 0', channels=None):
    channels = channels or [0, 1, 2, 3]
    return Fixture.objects.create(name=name, channels=channels)


def mocked_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json.dumps(json_data)
            self.status_code = status_code

        def json(self):
            return self.json_data

        @property
        def text(self):
            return self.json_data

    if args[0] == 'http://test/get_dmx':
        return MockResponse(json_data={"dmx": [150 for _ in range(512)]},
                            status_code=200)


def mocked_post(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json.dumps(json_data)
            self.status_code = status_code

    if args[0] == 'http://text/set_dmx':
        return MockResponse(json_data={}, status_code=200)


class OlaTest(TestCase):
    @mock.patch('requests.get', side_effect=mocked_get)
    def test_get_some_channels(self, mock_get):
        # Arrange
        ola_handler = OlaHandler(1, uri='http://test')

        # Act
        channels = ola_handler.get_values(start_channel=0, mapping={'0': 'color', '1': 'color', '2': 'color'})

        # Assert
        self.assertIn(mock.call('http://test/get_dmx', params={'u': 1}), mock_get.call_args_list)
        self.assertEqual({'color': '#969696'}, channels)

    @mock.patch('requests.post', side_effect=mocked_post)
    @mock.patch('requests.get', side_effect=mocked_get)
    def test_set_light_override_one_channel(self, mock_get, mock_post):
        # Arrange
        ola_handler = OlaHandler(1, uri='http://test')
        expected_channels = [11, 201, 17, 150, 150]
        expected_channels.extend(150 for _ in range(512 - len(expected_channels)))

        # Act
        ola_handler.set_color([0, 1, 2], "3ee54c")

        # Assert
        self.assertIn(mock.call('http://test/set_dmx',
                                data={'u': 1, 'd': ','.join(str(value)
                                                            for value in expected_channels)}),
                      mock_post.call_args_list)

    def test_gamma_correction(self):
        handler = OlaHandler(1)
        r, g, b = [255, 127, 255]

        corrected_color = handler._gamma_correction(r, g, b)

        self.assertEqual([255, 55, 255], corrected_color)

    def test_rgba_conversion(self):
        handler = OlaHandler(1)
        r, g, b, a = [255, 127, 255, 0.2]

        corrected_color = handler._convert_rgba_to_rgb(r, g, b, a)

        self.assertEqual([51, 25, 51], corrected_color)
