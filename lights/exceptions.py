from rest_framework import status
from rest_framework.exceptions import APIException


class InvalidChannelError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST


class OlaNotAvailableError(APIException):
    status_code = status.HTTP_503_SERVICE_UNAVAILABLE

    def __init__(self, *args, **kwargs):
        detail = args[0] if args else "Could not connect to OLA. Is the OLA server online?"
        super().__init__(detail=detail, *args, **kwargs)


class MethodNotImplementedError(APIException):
    status_code = status.HTTP_501_NOT_IMPLEMENTED


class InvalidParametersError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST


class InvalidMethodError(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
