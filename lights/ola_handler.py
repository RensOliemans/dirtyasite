import json

import requests

from lights.exceptions import (InvalidParametersError,
                               OlaNotAvailableError)


class OlaHandler:
    MAX_CHANNELS = 512
    DEFAULT_GAMMA = 2.2

    def __init__(self, universe_id, uri='http://residentiedirtya.student.utwente.nl:9090'):
        self.universe_id = universe_id
        self._current_channels = [0 for _ in range(self.MAX_CHANNELS)]
        self._uri = uri

    def set_color(self, channels, color, gamma=None):
        color_values = self._convert_hex_to_rgb(color)
        if len(channels) != len(color_values):
            raise InvalidParametersError(
                f"The given channels {channels} are not of the same length "
                f"of the color channels {color_values}")

        gamma = gamma or self.DEFAULT_GAMMA
        r, g, b = color_values
        color_values = self._gamma_correction(r, g, b, gamma)

        self._post_data(channels, color_values)

    def get_values(self, start_channel, mapping):
        method_mapping = self._invert_dict(mapping, start_channel)
        dmx_data = self._get_all_dmx_channels()

        return {
            method: getattr(self, method)(
                [dmx_data[channel] for channel in method_mapping[method]]
            ) for method in method_mapping
        }

    def toggle(self, channels, on):
        if len(channels) != 1:
            raise InvalidParametersError(f"Expected toggle to called with 1 channel, instead got "
                                         f"{len(channels)} channels.")

        channel_value = 255 if on else 0
        self._post_data(channels, [channel_value])

    def _post_data(self, channels, values):
        try:
            self._current_channels = self._get_all_dmx_channels()
            all_channels = self._create_channels(channels, values)
            self._set_dmx(all_channels)
        except requests.ConnectionError:
            raise OlaNotAvailableError()

    def _get_all_dmx_channels(self):
        data = {'u': self.universe_id}
        url = f"{self._uri}/get_dmx"

        r = requests.get(url, params=data)
        return json.loads(r.text)['dmx']

    def _create_channels(self, channels, values):
        return [values[channels.index(index)]
                if index in channels else value
                for index, value in enumerate(self._current_channels)]

    def _set_dmx(self, values):
        """
        Sets ola lights to a list of values
        :param values: list of values from 0 to 255
        """
        data = {'u': self.universe_id, 'd': ','.join(str(value) for value in values)}
        url = f"{self._uri}/set_dmx"

        r = requests.post(url, data=data)
        return r

    @staticmethod
    def _gamma_correction(r, g, b, gamma=2.2):
        return [int((color / 255)**gamma * 255) for color in [r, g, b]]

    @staticmethod
    def _convert_rgba_to_rgb(r, g, b, a, background=0):
        return [int((1 - a) * background + (a * color)) for color in [r, g, b]]

    @staticmethod
    def _invert_dict(dictionary, start_channel):
        return {value: [int(item) + int(start_channel)
                        for item in dictionary.keys()
                        if dictionary[item] == value]
                for key, value in dictionary.items()}

    @staticmethod
    def _convert_hex_to_rgb(color):
        color = color.lstrip('#')
        return list(int(color[i:i + 2], 16) for i in (0, 2, 4))

    @staticmethod
    def color(channels):
        """This method gets a list of channels and returns a value outputted to the user"""
        return f"#{channels[0]:02X}{channels[1]:02X}{channels[2]:02X}"

    @staticmethod
    def strobe(channels):
        return channels[0] == 255

    @staticmethod
    def music(channels):
        return channels[0] == 255
