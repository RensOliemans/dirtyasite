from rest_framework import viewsets, status, permissions
from rest_framework.decorators import action, api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.reverse import reverse

from lights.models import Fixture, Lamp
from lights.ola_handler import OlaHandler
from lights.serializers import FixtureSerializer, LampSerializer, ColorSerializer, ToggleSerializer

UNIVERSE_ID = 1

handler = OlaHandler(universe_id=UNIVERSE_ID)


class FixtureViewSet(viewsets.ModelViewSet):
    queryset = Fixture.objects.all()
    serializer_class = FixtureSerializer

    permission_classes = [permissions.IsAuthenticated]


class LampViewSet(viewsets.ModelViewSet):
    queryset = Lamp.objects.all()
    serializer_class = LampSerializer

    permission_classes = [permissions.IsAuthenticated]

    @action(methods=['get'], detail=True)
    def values(self, request, pk=None):
        """Gets the values of the light"""
        light = get_object_or_404(Lamp, pk=pk)
        values = handler.get_values(start_channel=light.start_channel,
                                    mapping=light.fixture.channel_method_mapping)
        return Response(values)

    @action(methods=['post'], detail=True, serializer_class=ColorSerializer)
    def color(self, request, pk):
        """
        Sets color. Give a 'color' in the post request, optionally a 'gamma'
        """
        light = get_object_or_404(Lamp, pk=pk)
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            color = serializer.validated_data.get('color')
            gamma = serializer.validated_data.get('gamma')

            channels = light.determine_channels(method_name='color')
            handler.set_color(channels, color, gamma)
            return Response(status=status.HTTP_200_OK)

    @action(methods=['post'], detail=True, serializer_class=ToggleSerializer)
    def strobe(self, request, pk):
        """
        Sets strobe. Takes a boolean
        """
        return self._toggle(request, pk, method_name='strobe')

    @action(methods=['post'], detail=True, serializer_class=ToggleSerializer)
    def music(self, request, pk):
        """
        Sets music. Takes a boolean.
        """
        return self._toggle(request, pk, method_name='music')

    def _toggle(self, request, pk, method_name):
        light = get_object_or_404(Lamp, pk=pk)
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            on = serializer.validated_data.get('on')

            channels = light.determine_channels(method_name=method_name)
            handler.toggle(channels, on)
            return Response(status=status.HTTP_200_OK)

    @action(methods=['get'], detail=True)
    def methods(self, request, pk):
        light = get_object_or_404(Lamp, pk=pk)
        return Response(light.methods)


@api_view(['GET'])
def lamp_root(request, format=None):
    return Response({
        'lamps': reverse('lights:lamp-list', request=request, format=format),
        'fixtures': reverse('lights:fixture-list', request=request, format=format),
    })
