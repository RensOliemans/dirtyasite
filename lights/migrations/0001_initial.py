# Generated by Django 2.2.6 on 2019-11-11 16:02

import django.contrib.postgres.fields
import django.contrib.postgres.fields.hstore
import django.core.validators
from django.contrib.postgres.operations import HStoreExtension
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        HStoreExtension(),
        migrations.CreateModel(
            name='Fixture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='The name of the fixture', max_length=20, unique=True)),
                ('methods', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(choices=[('color', 'color'), ('strobe', 'strobe'), ('music', 'music')], max_length=20), default=list, help_text='A list of methods which the fixture supports', size=None)),
                ('channel_method_mapping', django.contrib.postgres.fields.hstore.HStoreField(help_text='A mapping of channel (key) to method (value)')),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Lamp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('start_channel', models.PositiveIntegerField(unique=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(512)])),
                ('fixture', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='light', to='lights.Fixture')),
            ],
            options={
                'ordering': ('start_channel', 'name'),
            },
        ),
    ]
