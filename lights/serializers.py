from lights.models import Fixture, Lamp
from rest_framework import serializers
from lights.ola_handler import OlaHandler


class FixtureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fixture
        fields = ('id', 'name', 'channel_amount', 'methods', 'channel_method_mapping')


class LampSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lamp
        fields = ('id', 'name', 'fixture', 'methods', 'start_channel')


# The following serializers concern the post calls for methods
class ColorSerializer(serializers.Serializer):
    color = serializers.CharField(max_length=10, help_text="Hex value")
    gamma = serializers.FloatField(
        required=False,
        allow_null=True,
        help_text=f"Optional gamma value, defaults to {OlaHandler.DEFAULT_GAMMA}"
    )


class ToggleSerializer(serializers.Serializer):
    on = serializers.BooleanField()
