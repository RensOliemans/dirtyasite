# Website of Dirty-A.nl

Runs on Python3.7, using Django

## Getting started
* Zorg dat je een virtualenv hebt (python3.7 minimum) met schone libraries.
* Doe `pip install -r requirements.txt`.
* Maak een database (postgres) aan en onthoud de DB-naam, DB-user en 
  DB-password.
* Sla die variabelen op in een .env bestand (`DB_NAME`, `DB_USER`, 
  `DB_PASSWORD`). Sla het `.env` bestand op in `dirtyasite`, dezelfde
  directory als `settings.py`. Voeg daar ook een `SECRET_KEY` 
  toe, een 40-karakter string.
* Doe `python manage.py migrate`
  Als het goed is gaat dit gewoon goed, het zou kunnen dat je een beetje rond
  moet spelen met postgres extensies, we hebben namelijk wel een
  HStoreExtension nodig, wellicht dat de DB-user een superuser moet zijn om het
  uit te voeren. Vraag Ollie even als het niet lukt

## Filling some data
* `python manage.py createsuperuser`
* `python manage.py shell`
* `> import scripts.events_create_2020`
