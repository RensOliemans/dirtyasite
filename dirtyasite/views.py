from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'user': reverse('residents:resident-list', request=request, format=format),
        'event': reverse('events:event-list', request=request, format=format),
        'lights': reverse('lights:lights-root', request=request, format=format),
    })
