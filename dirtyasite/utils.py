from residents import model_factory as resident_factory
from events import model_factory as event_factory
from lights import model_factory as light_factory


def create_objects(amount_of_residents=8, amount_of_events=20,
                   amount_of_fixtures=5, amount_of_lamps=25):
    """
    Creates a shit ton of objects from all apps
    """
    for _ in range(amount_of_residents):
        resident_factory.ResidentFactory()

    for _ in range(amount_of_events):
        event_factory.EventFactory()

    for _ in range(int(amount_of_events * amount_of_residents / 3)):
        event_factory.RegistrationFactory()

    for _ in range(amount_of_fixtures):
        light_factory.FixtureFactory()

    for _ in range(amount_of_lamps):
        light_factory.LampFactory()
