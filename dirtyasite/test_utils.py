from django.utils import timezone

from events.models import Event
from residents.models import Resident, User

DEFAULT_DATE = timezone.now().date()


def create_event(title="Dinner", date=DEFAULT_DATE, deadline=DEFAULT_DATE, description="empty"):
    return Event.objects.create(title=title, date=date, deadline=deadline, description=description)


def create_resident(nickname="ollie", start=DEFAULT_DATE, room=9, birthday=DEFAULT_DATE, feut=False):
    user = User.objects.create(username=nickname)
    return Resident.objects.create(user=user, nickname=nickname, start=start, room=room, birthday=birthday, feut=feut)
