from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from dirtyasite import views
from django.contrib import admin
from django.urls import include, path


def build_schema(patterns):
    return get_schema_view(
        openapi.Info(
            title="Dirty-A API",
            default_version='v1',
            description='API van de huiswebsite',
            license=openapi.License(name='GNU'),
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
        patterns=patterns,
    )


app_patterns = [
    path('', views.api_root),

    path('residents/', include('residents.urls', namespace='residents')),
    path('events/', include('events.urls', namespace='events')),
    path('lights/', include('lights.urls', namespace='lights')),

    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns = [
    path('api/', include(app_patterns)),
    path('admin/', admin.site.urls),
]

urlpatterns += path('swagger/', build_schema(urlpatterns).with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
