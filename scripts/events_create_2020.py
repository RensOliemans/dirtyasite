from events.models import Event
from datetime import date, timedelta

start_date = date(2020, 1, 1)
for current_date in (start_date + timedelta(n) for n in range(366)):
    Event.objects.get_or_create(date=current_date)
